import csv

# Specify the input and output file paths
input_file = "LASKI.tsv"
output_file = "LASKI_fixed.tsv"

# Open the input file and filter rows
with open(input_file, "r", newline='', encoding="utf-8") as infile, \
        open(output_file, "w", newline='', encoding="utf-8") as outfile:
    reader = csv.reader(infile, delimiter='\t')
    writer = csv.writer(outfile, delimiter='\t')

    for row in reader:
        # Check if the first column is not empty
        if row[0].strip():  # Remove rows where the first column is empty
            writer.writerow(row)

print(f"Filtered rows have been saved to {output_file}")
